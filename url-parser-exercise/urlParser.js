const privateMethods = {
  getValue(value) {
    if (typeof value !== 'number') {
      return value;
    }
    return +value;
  },
  urlParserParameters(urlParameters) {
    return urlParameters.split('&')
      .reduce((result, item) => {
        const parts = item.split('=');// private method (can only be called within the class)
        result[parts[0]] = this.getValue(parts[1]); // eslint-disable-line no-param-reassign
        return result;
      }, {});
  },
};

module.exports = class UrlParser {
  constructor(partDelimiter = '/', partVariableDelimiter = ':', parameterDelimiter = '?') {
    this.partDelimiter = partDelimiter;
    this.partVariableDelimiter = partVariableDelimiter;
    this.parameterDelimiter = parameterDelimiter;
  }


  parse(urlFormat, urlInstance) {
    let result = {};
    const urlInstanceParameterList = urlInstance.split(this.parameterDelimiter);
    const urlInstanceNoParameter = urlInstanceParameterList[0];
    const urlInstanceParameter = urlInstanceParameterList[1];
    const partsFormatList = urlFormat.split(this.partDelimiter);
    const partsInstanceList = urlInstanceNoParameter.split(this.partDelimiter);
    for (let i = 1; i < partsFormatList.length; i += 1) {
      const partsFormatElement = partsFormatList[i];
      if (partsFormatElement.startsWith(this.partVariableDelimiter)) {
        result[partsFormatElement.substr(1)] = privateMethods.getValue(partsInstanceList[i]);
      }
    }
    const urlParserParametersObj = privateMethods.urlParserParameters(urlInstanceParameter);
    result = Object.assign(result, urlParserParametersObj);
    return result;
  }
};
