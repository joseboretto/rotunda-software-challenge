const MINUTE_IN_MILLISECOND = 6000;
// noinspection SpellCheckingInspection,JSUnusedLocalSymbols
const privateMethods = {
  // https://medium.com/@lazlojuly/are-node-js-modules-singletons-764ae97519af
  enableSendEmailEveryXMinute: true,
  errorQueue: null,
  getArrayWithLimitedLength(length) {
    const array = [];
    // eslint-disable-next-line func-names
    array.push = function (...args) {
      if (this.length >= length) {
        this.shift();
      }
      return Array.prototype.push.apply(this, args);
    };
    // eslint-disable-next-line func-names
    array.isFull = function () {
      return this.length === length;
    };
    return array;
  },
  /*
  * FAKE EMAIL NOTIFICATION
  */
  // eslint-disable-next-line no-unused-vars
  sendEmailNotification(error) {
    // console.error('sendEmailNotification', error);
  },
  sendEmailNotificationEveryXMinutes(error, minutes) {
    if (this.enableSendEmailEveryXMinute) {
      this.sendEmailNotification(error);
      this.enableSendEmailEveryXMinute = false;
      setTimeout(() => {
        this.enableSendEmailEveryXMinute = true;
      }, MINUTE_IN_MILLISECOND * minutes);
      return true;
    }
    return false;
  },
  checkEmailNotification(error, errorThresholdMinutes, errorQueue) {
    errorQueue.push(error);
    if (errorQueue.isFull()) {
      const firstError = errorQueue[0];
      const lastError = errorQueue[errorQueue.length - 1];
      const millis = (lastError.time - firstError.time);
      const minutes = Math.floor(millis / MINUTE_IN_MILLISECOND);
      if (minutes < errorThresholdMinutes) {
        return this.sendEmailNotificationEveryXMinutes(lastError);
      }
    }
    return false;
  },
};

module.exports = class EmailLogger {
  constructor(errorThresholdAmount = 10, errorThresholdMinutes = 1) {
    this.errorThresholdAmount = errorThresholdAmount;
    this.errorThresholdMinutes = errorThresholdMinutes;
    privateMethods.errorQueue = privateMethods.getArrayWithLimitedLength(this.errorThresholdAmount);
  }

  logError(error) {
    // eslint-disable-next-line max-len
    return privateMethods.checkEmailNotification(error, this.errorThresholdMinutes, privateMethods.errorQueue);
  }

  static getErrorQueueLength() {
    return privateMethods.errorQueue.length;
  }
};
