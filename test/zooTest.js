const assert = require('assert');
const { describe } = require('mocha');
const { it } = require('mocha');
const Lion = require('../zoo-exercise/lion');
const Tiger = require('../zoo-exercise/tiger');


describe('Lion', () => {
  describe('#speak()', () => {
    it('should add roar at the end of every word', () => {
      const lion = new Lion();
      const result = lion.speak("I'm a lion");
      const expectedResult = "I'm roar a roar lion roar";
      assert.equal(result, expectedResult);
    });
  });
});

describe('Tiger', () => {
  describe('#speak()', () => {
    it('should add grrr at the end of every word', () => {
      const tiger = new Tiger();
      const result = tiger.speak('Lions suck');
      const expectedResult = 'Lions grrr suck grrr';
      assert.equal(result, expectedResult);
    });
  });
});
