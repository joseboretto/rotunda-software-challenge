const assert = require('assert');
const { describe } = require('mocha');
const { it } = require('mocha');
const UrlParser = require('../url-parser-exercise/urlParser');


describe('UrlParser', () => {
  describe('#parse()', () => {
    it('should work when last part of url format is a constant', () => {
      const urlFormat = '/:version/api/:collection/:id';
      const urlInstance = '/6/api/listings/3?sort=desc&limit=10';
      const urlParser = new UrlParser();
      const result = urlParser.parse(urlFormat, urlInstance);
      const expectedResult = {
        version: 6,
        collection: 'listings',
        id: 3,
        sort: 'desc',
        limit: 10,
      };
      assert.equal(result.version, expectedResult.version);
      assert.equal(result.collection, expectedResult.collection);
      assert.equal(result.id, expectedResult.id);
      assert.equal(result.sort, expectedResult.sort);
      assert.equal(result.limit, expectedResult.limit);
    });
    it('should work when last part of url format is a parameter', () => {
      const urlFormat = '/:version/api/collection/id';
      const urlInstance = '/6/api/listings/3?sort=desc&limit=10';
      const urlParser = new UrlParser();
      const result = urlParser.parse(urlFormat, urlInstance);
      const expectedResult = { version: 6, sort: 'desc', limit: 10 };
      assert.equal(result.version, expectedResult.version);
      assert.equal(result.sort, expectedResult.sort);
      assert.equal(result.limit, expectedResult.limit);
    });
  });
});
