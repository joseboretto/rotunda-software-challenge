const assert = require('assert');
const { describe } = require('mocha');
const { it } = require('mocha');
const EmailLogger = require('../error-alarm-exercise/emailLogger');


describe('EmailLogger', () => {
  describe('#logError()', () => {
    it('should send email when 10 error happens in less than a minute', () => {
      const emailLogger = new EmailLogger();
      for (let i = 1; i < 15; i += 1) {
        const date = new Date();
        const error = {
          msg: i,
          time: date,
        };
        const logError = emailLogger.logError(error);
        if (i === 10) {
          assert.equal(logError, true);
        } else {
          assert.equal(logError, false);
        }
      }
    });
    it('should not send email when 10 error happens in more than a minute', () => {
      const emailLogger = new EmailLogger();
      for (let i = 1; i < 15; i += 1) {
        const date = new Date();
        date.setMinutes(date.getMinutes() + (i));
        const error = {
          msg: i,
          time: date,
        };
        const logError = emailLogger.logError(error);
        assert.equal(logError, false);
      }
    });
  });
});
