<h1 align="center">Welcome to rotunda-software-challenge 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: ISC" src="https://img.shields.io/badge/License-ISC-yellow.svg" />
  </a>
</p>

> Challenge for https://www.rotundasoftware.com/ as Full Stack JavaScript Developer https://apply.workable.com/rotunda-software/j/8B6919B500/

### 🏠 [Homepage](https://gitlab.com/joseboretto/rotunda-software-challenge#readme)

## Install

```sh
npm install
```

## Run tests

```sh
npm run test
```

## Author

👤 **Jose Boretto**


## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/joseboretto/rotunda-software-challenge/issues). You can also take a look at the [contributing guide](https://gitlab.com/joseboretto/rotunda-software-challenge/blob/master/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
