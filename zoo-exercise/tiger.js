const Animal = require('./animal');

class Lion extends Animal {
  constructor(sound = 'grrr') {
    super(sound);
  }
}
module.exports = Lion;
