class Animal {
  constructor(sound) {
    this.sound = sound;
  }

  speak(text) {
    return `${text.replace(/\s/g, ` ${this.sound} `)} ${this.sound}`;
  }
}
module.exports = Animal;
