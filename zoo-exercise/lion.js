const Animal = require('./animal');

class Lion extends Animal {
  constructor(sound = 'roar') {
    super(sound);
  }
}
module.exports = Lion;
